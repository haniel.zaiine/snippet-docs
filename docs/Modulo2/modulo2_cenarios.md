# Módulo 2

### Cenário 1: Lançamento com Y
    
    Dado: Estou na tela x 
    Quando: For realizada y ação  
    Entao: eu devo obter z resultado

### Cenário 2: Caso especifico
    
    Dado: Estou na tela x 
    Quando: For realizada y ação  
    Entao: eu devo obter z resultado

### Cenário 3: Exclusão do registro Y
    
    Dado: Estou na tela x 
    Quando: For realizada y ação  
    Entao: eu devo obter z resultado

### Cenário 4: Devolução de material
    
    Dado: Estou na tela x 
    Quando: For realizada y ação  
    Entao: eu devo obter z resultado

### Cenário 5: Apenas mais um cenário  
    
    Dado: Estou na tela x 
    Quando: For realizada y ação  
    Entao: eu devo obter z resultado