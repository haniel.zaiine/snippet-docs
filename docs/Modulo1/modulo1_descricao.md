# Módulo 1

### Descrição do módulo1 e suas funcionalidades. 

| Exemplo   | Valor do exemplo |
|-----------|------------------|
| Exemplo 1 | R$ 10            |
| Exemplo 2 | R$ 8             |
| Exemplo 3 | R$ 7             |
| Exemplo 4 | R$ 8             |

### Padrão de tela 
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque vulputate lorem porttitor leo suscipit, vestibulum feugiat nisl rhoncus. Nullam faucibus odio et diam blandit facilisis. Nunc a tincidunt ligula. Nulla tincidunt ex eu leo tincidunt egestas. Mauris feugiat felis sit amet turpis mattis interdum. Pellentesque tempor erat ipsum, ut congue nisi accumsan convallis.
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque vulputate lorem porttitor leo suscipit, vestibulum feugiat nisl rhoncus. Nullam faucibus odio et diam blandit facilisis. Nunc a tincidunt ligula. Nulla tincidunt ex eu leo tincidunt egestas. Mauris feugiat felis sit amet turpis mattis interdum. Pellentesque tempor erat ipsum, ut congue nisi accumsan convallis.

!!! example "Seguir padrao da tela X."
    Seguir formato de exibição da tela x.

### Filtros 
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque vulputate lorem porttitor leo suscipit, vestibulum feugiat nisl rhoncus. Nullam faucibus odio et diam blandit facilisis. Nunc a tincidunt ligula. Nulla tincidunt ex eu leo tincidunt egestas. Mauris feugiat felis sit amet turpis mattis interdum. Pellentesque tempor erat ipsum, ut congue nisi accumsan convallis.

!!! warning "Tipo de dado X não deve ser exibido no filtro."
    Esse tipo de dado não deve exibir pois essa funcionalidade não se aplica e essa categoria de Fulanos. 

