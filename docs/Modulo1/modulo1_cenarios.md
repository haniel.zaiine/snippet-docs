# Módulo 1

### Cenário 1: Venda com X 
    
    Dado: Estou na tela x 
    Quando: For realizada y ação  
    Entao: eu devo obter z resultado

### Cenário 2: Produto sem estoque
    
    Dado: Estou na tela x 
    Quando: For realizada y ação  
    Entao: eu devo obter z resultado

### Cenário 3: Vencimento de lote de produtos
    
    Dado: Estou na tela x 
    Quando: For realizada y ação  
    Entao: eu devo obter z resultado

### Cenário 4: Venda de produtos com estoque negativo
    
    Dado: Estou na tela x 
    Quando: For realizada y ação  
    Entao: eu devo obter z resultado

### Cenário 5: Apenas mais um cenário  
    
    Dado: Estou na tela x 
    Quando: For realizada y ação  
    Entao: eu devo obter z resultado